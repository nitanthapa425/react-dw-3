import React, { useContext } from "react";
import { CountContext, InfoContext } from "../../Nitan";

const LearnUseContext1 = () => {
  let data = useContext(InfoContext);

  let c1 = useContext(CountContext);
  return (
    <div>
      {data}
      <br></br>
      count = {c1.count}
      <br></br>
      LearnUseContext1
      <br></br>
      <button
        onClick={() => {
          c1.setCount(c1.count + 1);
        }}
      >
        Increment
      </button>
    </div>
  );
};

export default LearnUseContext1;
