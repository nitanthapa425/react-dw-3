import React, { useContext } from "react";
import { AgeContext } from "../../Nitan";

const LearnUseContext2 = () => {
  let age = useContext(AgeContext);
  return (
    <div>
      Age is {age}
      <br></br>
      LearnUseContext2
    </div>
  );
};

export default LearnUseContext2;
