import React, { useContext } from "react";
import LearnUseContext1 from "./LearnUseContext1";
import { InfoContext } from "../../Nitan";

const LearnUseContext = () => {
  let data = useContext(InfoContext);
  return (
    <div>
      {data}
      <br></br>
      LearnUseContext
      <LearnUseContext1></LearnUseContext1>
    </div>
  );
};

export default LearnUseContext;
