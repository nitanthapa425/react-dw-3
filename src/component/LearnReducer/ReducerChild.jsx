import React from "react";

const ReducerChild = () => {
  console.log("checking reducer child will change");
  return <div>ReducerChild</div>;
};

export default ReducerChild;
