import React, { useReducer } from "react";
import ReducerChild from "./ReducerChild";

const Main = () => {
  let initialValue = 0;
  let reducer = (state, action) => {
    if (action === "increment") return state + 1;
    else if (action === "decrement") return state - 1;
    return state;
  };

  console.log("run");

  //   when dispatch function is called reducer function gets execute
  let [state, dispatch] = useReducer(reducer, initialValue);
  return (
    <div>
      {state}

      <button
        onClick={() => {
          dispatch("decrement");
        }}
      >
        Increment
      </button>
      <button
        onClick={() => {
          dispatch();
        }}
      >
        none
      </button>

      <ReducerChild></ReducerChild>
    </div>
  );
};

export default Main;
