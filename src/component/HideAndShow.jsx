import React, { useState } from "react";

const HideAndShow = () => {
  let [show, setShow] = useState(true);
  return (
    <div>
      {show ? <p>Error has occured</p> : null}

      <button
        onClick={() => {
          //   setShow(false);
          setShow(!show);
        }}
      >
        toggle
      </button>
    </div>
  );
};

export default HideAndShow;
