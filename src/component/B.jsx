import React from "react";
import C from "./C";

const B = ({ count }) => {
  return (
    <div>
      B<C count={count}></C>
    </div>
  );
};

export default B;
