import React, { useRef } from "react";

const LearnUseRef = () => {
  let inputRef1 = useRef();
  let inputRef2 = useRef();
  let inputRef3 = useRef();
  return (
    <div>
      <p ref={inputRef1}>p1</p>
      <p ref={inputRef2}>p2</p>

      <button
        onClick={() => {
          inputRef1.current.style.backgroundColor = "red";
          inputRef2.current.style.backgroundColor = "blue";
        }}
      >
        change bg of p
      </button>
      <br></br>

      <input ref={inputRef3}></input>
      <br></br>

      <button
        onClick={() => {
          inputRef3.current.focus();
        }}
      >
        Focus input
      </button>
    </div>
  );
};

export default LearnUseRef;
