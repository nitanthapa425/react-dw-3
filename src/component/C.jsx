import React from "react";

const C = ({ count }) => {
  return <div>C{count}</div>;
};

export default C;
