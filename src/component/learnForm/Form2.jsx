import React, { useState } from "react";

const Form2 = () => {
  let [name, setName] = useState("");
  let [address, setAddress] = useState("");
  let [description, setDescription] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let [country, setCountry] = useState("nepal");
  let [gender, setGender] = useState("male");

  let countries = [
    { label: "Select Country", value: "", disabled: true },
    { label: "Nepal", value: "nepal" },
    { label: "China", value: "china" },
    { label: "India", value: "india" },
    { label: "America", value: "america" },
  ];

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        let info = {
          name: name,
          address: address,
          description: description,
          isMarried: isMarried,
          country,
          gender,
        };
        console.log(info);
      }}
    >
      <br></br>
      <label htmlFor="name">Name: </label>
      <input
        id="name"
        type="text"
        placeholder="Enter your name"
        value={name}
        onChange={(e) => {
          setName(e.target.value);
        }}
      ></input>
      <br></br>
      <label htmlFor="address">Address</label>
      <input
        id="address"
        type="text"
        placeholder="Enter your address"
        value={address}
        onChange={(e) => {
          setAddress(e.target.value);
        }}
      ></input>
      <br></br>
      <label htmlFor="description">Description</label>
      <textarea
        id="description"
        placeholder="Enter your description"
        value={description}
        onChange={(e) => {
          setDescription(e.target.value);
        }}
        // rows={10}
        // cols={50}
      ></textarea>
      <br></br>
      <label htmlFor="isMarried">IsMarried</label>
      <input
        id="isMarried"
        type="checkbox"
        checked={isMarried}
        onChange={(e) => {
          setIsMarried(e.target.checked);
        }}
      ></input>
      <br></br>

      <label htmlFor="country">Country </label>
      <select
        id="country"
        value={country}
        onClick={(e) => {
          setCountry(e.target.value);
        }}
      >
        {countries.map((item, i) => (
          <option value={item.value} disabled={item.disabled}>
            {item.label}
          </option>
        ))}
      </select>
      <br></br>

      {/* <label htmlFor="male">Gender </label>

      <label htmlFor="male">Male</label>
      <input
        checked={gender === "male"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
        type="radio"
        id="male"
        value="male"
      ></input>

      <label htmlFor="female">Female</label>
      <input
        type="radio"
        id="female"
        value="female"
        checked={gender === "female"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
      ></input>

      <label htmlFor="other">Other</label>
      <input
        type="radio"
        id="other"
        value="other"
        checked={gender === "other"}
        onChange={(e) => {
          setGender(e.target.value);
        }}
      ></input>

      <br></br> */}

      <label htmlFor="male">Gender </label>
      {genders.map((item, i) => {
        return (
          <>
            <label htmlFor={item.value}>{item.label} </label>
            <input
              checked={gender === item.value}
              onChange={(e) => {
                setGender(e.target.value);
              }}
              type="checkbox"
              id={item.value}
              value={item.value}
            ></input>
          </>
        );
      })}

      <br></br>

      <button type="submit">Send</button>
    </form>
  );
};

export default Form2;
