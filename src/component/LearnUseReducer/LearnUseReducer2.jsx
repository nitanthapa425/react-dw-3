import React, { useReducer } from "react";

const LearnUseReducer2 = () => {
  let initialvalues = 0;

  let reducer = (state, action) => {
    // it must return next state value
    if (action.type === "increment") {
      return state + 1;
    } else if (action.type === "decrement") {
      return state - 1;
    } else if (action.type === "reset") {
      return initialvalues;
    }

    return state;
  };

  let [count, dispatch] = useReducer(reducer, initialvalues);

  return (
    <div>
      {count}
      <br></br>
      <button
        onClick={() => {
          dispatch({ type: "increment" });
        }}
      >
        Increment
      </button>
      <br></br>
      <button
        onClick={() => {
          dispatch({ type: "decrement" });
        }}
      >
        Decrement
      </button>
      <br></br>
      <button
        onClick={() => {
          dispatch({ type: "reset" });
        }}
      >
        Reset
      </button>
      <br></br>
    </div>
  );
};

export default LearnUseReducer2;
