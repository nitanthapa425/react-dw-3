import React from "react";
import useGlobalContextHook from "./useGlobalContextHook";

const Sibling3 = () => {
  let data = useGlobalContextHook();

  return <div>Sibling2 {JSON.stringify(data)}</div>;
};

export default Sibling3;
