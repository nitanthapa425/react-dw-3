import React from "react";
import Parent from "./Parent";
import useGlobalContextHook from "./useGlobalContextHook";

const GrandParent = () => {
  let data = useGlobalContextHook();

  return (
    <div>
      I am Grand Parent and value is {JSON.stringify(data)}
      <Parent></Parent>
    </div>
  );
};

export default GrandParent;
