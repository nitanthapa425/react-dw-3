import React from "react";
import useGlobalContextHook from "./useGlobalContextHook";

const Sibling1 = () => {
  let data = useGlobalContextHook();
  return <div>Sibling1 {JSON.stringify(data)}</div>;
};

export default Sibling1;
