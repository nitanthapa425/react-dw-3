import React from "react";

import useGlobalContextHook from "./useGlobalContextHook";

const Child = () => {
  let data = useGlobalContextHook();
  return <div>Child {JSON.stringify(data)}</div>;
};

export default Child;
