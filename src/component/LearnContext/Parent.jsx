import React from "react";
import Child from "./Child";
import useGlobalContextHook from "./useGlobalContextHook";

const Parent = () => {
  let data = useGlobalContextHook();

  return (
    <div>
      I am Parent {JSON.stringify(data)}
      <Child></Child>
      <button
        onClick={() => {
          data.setInfo((pre) => {
            return { ...pre, name: "hari" };
          });
        }}
      >
        {" "}
        Child Button
      </button>
    </div>
  );
};

export default Parent;
