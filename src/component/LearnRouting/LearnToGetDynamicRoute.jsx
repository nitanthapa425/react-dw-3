import React from "react";
import { useParams } from "react-router-dom";

const LearnToGetDynamicRoute = () => {
  let params = useParams();

  console.log(params);
  return <div>LearnToGetDynamicRoute</div>;
};

export default LearnToGetDynamicRoute;
