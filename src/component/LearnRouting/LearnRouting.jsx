import React from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import NavLinks from "./NavLinks";
import Routess from "./Routess";

const LearnRouting = () => {
  return (
    <div>
      <NavLinks></NavLinks>
      <Routess></Routess>
    </div>
  );
};

export default LearnRouting;
