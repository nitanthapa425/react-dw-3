import React from "react";
import { useSearchParams } from "react-router-dom";

const LearnToGetSearchParams = () => {
  //  let params= useParams();
  let [params] = useSearchParams();

  let name = params.get("name");

  let age = params.get("age");

  return (
    <div>
      name is {name}
      <br></br>
      age is {age}
      <br></br>
    </div>
  );
};

export default LearnToGetSearchParams;
