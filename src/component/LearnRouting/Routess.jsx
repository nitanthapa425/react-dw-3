import React from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import CommonContact from "./CommonContact";
import LearnToGetDynamicRoute from "./LearnToGetDynamicRoute";
import LearnToGetSearchParams from "./LearnToGetSearchParams";

const Routess = () => {
  // define component for each url
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/about" element={<About></About>}></Route>
        <Route path="/contact" element={<Contact></Contact>}></Route>
        <Route
          path="/contact/:a"
          element={<CommonContact></CommonContact>}
        ></Route>
        <Route path="/contact/1" element={<div>this is contact 1</div>}></Route>

        <Route
          path="/contact/:id1/id/:id2"
          element={<LearnToGetDynamicRoute></LearnToGetDynamicRoute>}
        ></Route>

        <Route
          path="/product"
          element={<LearnToGetSearchParams></LearnToGetSearchParams>}
        ></Route>
      </Routes>
    </div>
  );
};

export default Routess;
