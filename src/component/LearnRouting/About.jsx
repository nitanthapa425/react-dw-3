import React from "react";
import { useNavigate } from "react-router-dom";

const About = () => {
  let navigate = useNavigate();
  return (
    <div>
      About
      <button
        onClick={() => {
          // navigate("/contact");
          navigate("/contact", { replace: true });
        }}
      >
        go to contact
      </button>
    </div>
  );
};

export default About;
