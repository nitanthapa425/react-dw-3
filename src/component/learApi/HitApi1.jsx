import axios from "axios";
import React from "react";

const HitApi1 = () => {
  let ReadAllContact = async () => {
    let obj = {
      url: "https://fake-api-nkzv.onrender.com/api/v1/contacts",
      method: "get",
    };
    let result = await axios(obj);

    console.log(result.data.data.results);
  };

  let createContact = async () => {
    let obj1 = {
      url: "https://fake-api-nkzv.onrender.com/api/v1/contacts",
      method: "POST",
      data: {
        fullName: "nitan",
        address: "gagalphedi",
        phoneNumber: 9849468999,
        email: "nitanthapa425@gmail.com",
      },
    };
    let result1 = await axios(obj1);
    console.log(result1);
  };

  return (
    <div>
      <button
        onClick={() => {
          ReadAllContact();
        }}
      >
        Read All Contact
      </button>

      <button
        onClick={() => {
          createContact();
        }}
      >
        Create New Contact
      </button>
    </div>
  );
};

export default HitApi1;
