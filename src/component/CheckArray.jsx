import React, { useState } from "react";

const CheckArray = () => {
  let count = 1;
  return (
    <div>
      count is {count}
      <br></br>
      <button
        onClick={() => {
          count = 2;
        }}
      >
        click me
      </button>
      count is {count}
    </div>
  );
};

export default CheckArray;

// import React, { useState } from "react";

// const CheckArray = () => {
//   let [count, setCount] = useState(1);
//   return (
//     <div>
//       count is {count}
//       <br></br>
//       <button
//         onClick={() => {
//           setCount(2);
//         }}
//       >
//         click me
//       </button>
//     </div>
//   );
// };

// export default CheckArray;
