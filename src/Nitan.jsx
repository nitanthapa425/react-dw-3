import React, { createContext, useState } from "react";
import Counter from "./component/Counter";
import HideAndShow from "./component/HideAndShow";
import CheckArray from "./component/CheckArray";
import A from "./component/A";
import LearnUseEffect from "./component/LearnUseEffect";
import LearnUseEffect2 from "./component/LearnUseEffect2";
import LearnChildrenProps from "./component/LearnChildrenProps";
import Form1 from "./component/learnForm/Form1";
import Form2 from "./component/learnForm/Form2";
import HitApi1 from "./component/learApi/HitApi1";
import ContactForm from "./component/contact/ContactForm";
import ReadAllContact from "./component/contact/ReadAllContact";
import AdvanceuseState from "./component/AdvanceuseState";
import LearnUseRef from "./component/LearnUseRef";
import GrandParent from "./component/LearnContext/GrandParent";
import Sibling1 from "./component/LearnContext/Sibling1";
import Sibling3 from "./component/LearnContext/Sibling2";
import Main from "./component/LearnReducer/Main";
import LearnUseReducer from "./component/LearnUseReducer/LearnUseReducer";
import LearnUseReducer2 from "./component/LearnUseReducer/LearnUseReducer2";
import LearnUseContext from "./component/LearnUseContext/LearnUseContext";
import LearnUseContext2 from "./component/LearnUseContext/LearnUseContext2";
import LearnRouting from "./component/LearnRouting/LearnRouting";

export let InfoContext = createContext();
export let AgeContext = createContext();
export let CountContext = createContext();
const Nitan = () => {
  let [showComponent, setShowComponent] = useState(true);

  // console.log("i am parent");

  let [info, setInfo] = useState({
    name: "nitan",
    age: 29,
    isMarried: false,
  });

  let [name1, setName1] = useState("nitan");
  let [age, setAge] = useState(29);

  let [count, setCount] = useState(0);

  return (
    <div>
      {/* <Counter></Counter> */}
      {/* <HideAndShow></HideAndShow> */}
      {/* <CheckArray></CheckArray> */}
      {/* <A></A> */}
      {/* <LearnUseEffect></LearnUseEffect> */}

      {/* {showComponent ? <LearnUseEffect2></LearnUseEffect2> : null} */}
      {/* or */}
      {/* {showComponent && <LearnUseEffect2></LearnUseEffect2>}
      <button
        onClick={() => {
          setShowComponent(false);
        }}
      >
        {" "}
        hide useEffect
      </button> */}

      {/* <LearnChildrenProps name={"nitan"} age={29}>
        <div style={{ backgroundColor: "chartreuse" }}>
          <p>this is paragraph1</p>
          <p>this is paragrph2</p>
        </div>
        <HideAndShow></HideAndShow>
      </LearnChildrenProps> */}

      {/* <div style={{ backgroundColor: "chartreuse" }}>
        <p>this is paragraph1</p>
        <p>this is paragrph2</p>
      </div>
      <div className="bg-red">
        <p>this is paragraph1</p>
        <p>this is paragrph2</p>
      </div> */}
      {/* <Form1></Form1> */}
      {/* <Form2></Form2> */}
      {/* <HitApi1></HitApi1> */}
      {/* <ContactForm></ContactForm> */}
      {/* <ReadAllContact></ReadAllContact> */}

      {/* <AdvanceuseState></AdvanceuseState> */}

      {/* <LearnUseRef></LearnUseRef> */}

      {/* learn context */}

      {/* <Main></Main> */}

      {/* <LearnUseReducer></LearnUseReducer> */}

      {/* <LearnUseReducer2></LearnUseReducer2> */}

      {/* <AgeContext.Provider value={age}>
        <InfoContext.Provider value={name1}>
          <CountContext.Provider value={{ count: count, setCount: setCount }}>
            <LearnUseContext></LearnUseContext>
          </CountContext.Provider>

          <LearnUseContext2></LearnUseContext2>
        </InfoContext.Provider>
      </AgeContext.Provider> */}

      <LearnRouting></LearnRouting>
    </div>
  );
};

export default Nitan;
